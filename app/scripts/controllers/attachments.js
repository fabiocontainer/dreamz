'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .controller('AttachmentsCtrlModal', ['API_Configs', '$scope','$rootScope', '$modalInstance', 'initParams', 'Files', 'FileUploader', 'localStorageService', function (API_Configs, $scope, $rootScope, $modalInstance, initParams, Files, FileUploader, localStorageService){
  $scope.files = [];
  $scope.fileTypes = ["FILE", "URL"];
  $scope.mode = "upload";
  $scope.selecteds = [];
  $scope.file = {};
  $scope.searchQuery = null;
  $scope.searchType = null;
  $scope.totalItens = 0;
  $scope.perPage = 12;
  $scope.currentPage = 0;
  
  $scope.fileURI = "";
  $scope.preview = ""
  
  $scope.$watch("fileURI", function (value, oldValue){

    if(value){
      var rendered = false;
      if(/(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(value)){
        var matches = /(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(value);
        
        $("#previewVid").html("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/"+matches[2]+"\" frameborder=\"0\" allowfullscreen></iframe>");
        var rendered = true;
      }
      if(/(.+)\.dailymotion.com\/video\/([0-9a-z]+)_(.+)/i.exec(value)){
        var matches = /(.+)\.dailymotion.com\/video\/([0-9a-z]+)_(.+)/i.exec(value);
        $("#previewVid").html('<iframe frameborder="0" width="560" height="315" src="//www.dailymotion.com/embed/video/'+matches[2]+'" allowfullscreen></iframe>');
        var rendered = true;
      }
      
      if(/(.+)vimeo.com\/([0-9]+)/i.exec(value)){
        var matches = /(.+)vimeo.com\/([0-9]+)/i.exec(value);
        $("#previewVid").html('<iframe src="https://player.vimeo.com/video/'+matches[2]+'" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
        var rendered = true;
      }
      
      if(rendered == false){
        window.alert("Fonte de vídeo não Autorizada. Falar com o Programador");
      }
      
      
    } else {
      $("#previewVid").html('');
    }
  })
  
  $scope.saveURI = function ($event){
    $event.preventDefault();
    Files.create({}, {urls: [$scope.fileURI]}).$promise.then(function (data){
      $scope.fileURI = "";
       $("#previewVid").html('');
      $scope.changeMode('library')
      $scope.loadLibrary(0, null);
    })
  }
  
  $scope.pagination = {startPages: 0, endPages: 0};
  
  var token = localStorageService.get("Dreamz_token");
  var headers = {};
	if(token){
		var headers = {Authorization: 'Bearer ' + token}
	}
	$scope.$watch("currentPage", function (val){
	  if($scope.totalPages){
	    $scope.pagination.startPages = Math.max(0, ($scope.currentPage-3));
  	  $scope.pagination.endPages = Math.min(($scope.totalPages-1), ($scope.currentPage+3));
	  }
	})
	$scope.$watch("totalItens", function (val){
	  $scope.totalPages = Math.ceil($scope.totalItens / $scope.perPage);
	  $scope.currentPage = 0;
	})
	
	
	
  $scope.uploader = new FileUploader({
    url: API_Configs.location+"/files",
    alias: "files",
    autoUpload: true,
    'headers': headers,
    queueLimit: 5,
    onErrorItem: function(item, response, status, headers){
      console.log(item)
      console.log(response)
      console.log(status)
      console.log(headers)
    },
    onCompleteAll: function (){
      $scope.changeMode('library')
      $scope.loadLibrary(0, null);
    }
  });
  
  
  var count = (!initParams || !(initParams.counts))?9999999:initParams.counts;
  var returnWatch = (!initParams || !(initParams.callBack))?"MEDIA_SELECTED":initParams.callBack;
  
  $scope.loadLibrary = function (current, search, type){
    var params = {perPage: $scope.perPage, 'page': 0};
    if(search){
      params.q = search;
    }
    if(type){
      params.type = type;
    }
    params.page = current;
    
    return Files.query(params).$promise.then(function (data){
      $scope.totalItens = data.count;
      $scope.files = data.rows;
    })
  }
  
  $scope.searchLibrary = function ($event){
    $scope.loadLibrary(0, $scope.searchQuery, $scope.searchType)
    $event.preventDefault();
  }
  
  $scope.loadLibrary(0, null);
  $scope.isSelected = function (item){
    for(var i in $scope.selecteds){
      var sel = $scope.selecteds[i];
      if(sel.id == item.id){
        return true;
      }
    }
    return false;
  }
  $scope.removeItem = function (item){
    for(var i in $scope.selecteds){
      var sel = $scope.selecteds[i];
      if(sel.id == item.id){
        $scope.selecteds.splice(i, 1);
        return true;
      }
    }
    return false;
  }
  $scope.setSelected = function (item){
    $scope.file = item;
    angular.element(".fileList #item-"+item.id).addClass("selected")
    //angular.element("#mediaBrowser #fileAr").removeClass("col-lg-12").addClass("col-lg-9")
  }
  $scope.setUnselected = function (item){
    angular.element(".fileList #item-"+item.id).removeClass("selected")
    //angular.element("#mediaBrowser #fileAr").removeClass("col-lg-9").addClass("col-lg-12")
  }
  $scope.addSelectedItem = function (item){
    if(count  == 1){
      $scope.selecteds = [item];
      angular.element(".fileList li").removeClass("selected");
      $scope.setSelected(item)
    } else {
      if($scope.isSelected(item)){
        $scope.removeItem(item);
        $scope.setUnselected(item);
      } else {
          if($scope.selecteds.length+1 <= count){
            $scope.selecteds.push(item)
            $scope.setSelected(item)
          } else {
            window.alert("Maximun elements")
          }
      }
    }
  }
  
  $scope.goPage = function (page, $event){
    $scope.currentPage = page;
    $scope.loadLibrary($scope.currentPage, $scope.searchQuery, $scope.searchType);
    $event.preventDefault();
  }
  
  $scope.changeMode = function (mode, event){
    if(event){ event.preventDefault(); }
    $scope.mode = mode;
    return false;
  }
  
  $scope.selectPhotos = function(){
    $rootScope.$broadcast(returnWatch, $scope.selecteds);
    $scope.closeLight();
  }
  $scope.closeLight = function (){
    $modalInstance.close();
  }
}])

