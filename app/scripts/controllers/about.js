'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
