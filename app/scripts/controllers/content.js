'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('ContentCtrl', function ($scope, $route, Content, Editorials, Tags, $location, $rootScope, localStorageService, $modal, $q, Question) {
    var $this = this;
    $scope.editorials = [];
    $scope.vidEditorial = null;
    $scope.content_types = [{name: 'NEWS', id: 'news'}, {name: 'GALLERY', id: 'gallery'}, {name: 'QUIZ', id: 'quiz'}, {name: 'RANKING', id: 'ranking'}]
    $scope.content = {type: 'news', editorials: [], status: 'draft', extras: []};
    
    $rootScope.$watch("currentSite", function (nValue, value){
      if(nValue){
        Editorials.editorials({site: $rootScope.currentSite.id}).$promise.then(function (data){
          $scope.editorials = data;
          
          for(var i in data){
            if(data[i]['url'] == "videos/"){
              $scope.vidEditorial = data[i];
            }
          }
          
        })
      }
    })
    
    $scope.modifyHtml = function (html){
      console.log(html);
      
    }
    
    
    
    
    //Related Content
    $scope.openRelatedContent = function ($event){
      if($event.preventDefault){ $event.preventDefault() }
      var $alertView = $modal.open({
        templateUrl: "views/tpl/relatedModal.html",
        controller: "ContentCtrlModal",
        size: "lg",
        resolve: {
          initParams: function (){
            return {type: "YES/NO", message: "ARE_YOU_SURE_WANT_TO_REMOVE_QUESTION", title: "CAUTION!"}
          }
        },
      })
      $alertView.result.then(function (change){
        if(!$scope.content.extras){
          $scope.content.extras = []
        }
        if(change.type == "OK" && change.item){
          $scope.content.extras.push(change.item);
        }
      })
      
      return false
    }
    $scope.removeContentRel = function(item, $event){
      if($event.preventDefault){ $event.preventDefault() }
      
      $scope.content.extras = $scope.content.extras.filter(function (content){
        if(content != item){
          return content;
        }
      })
    }
    
    
    // Quizz Tools
    $scope.addResult = function ($event){
      if(!$scope.content.Results){
        $scope.content.Results = [];
      }
      
      $scope.content.Results.push({title: "", fullText: ""});
      $event.preventDefault();
    }
    $scope.addQuestion = function ($event){
      if(!$scope.content.Questions){
        $scope.content.Questions = [];
      }
      $scope.content.Questions.push({title: "", fullText: "", Answers: [{title: "", fullText: ""}]})
      $event.preventDefault();
    }
    $scope.addAnswer = function(item, $event){
      if(!item.Answers){
        item.Answers = [];
      }
      item.Answers.push({title: "", fullText: ""});
      $event.preventDefault();
    }
    $scope.addImageQuestion = function (question, $event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "QUIZ_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("QUIZ_SELECT", function (ev, item){
        if(item){
          question.image = item[0];
        }
        eventCalled()
      })
      $event.preventDefault();
    }
    $scope.addImageResult = function (result, $event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "RESULT_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("RESULT_SELECT", function (ev, item){
        if(item){
          result.image = item[0];
        }
        eventCalled()
      })
      $event.preventDefault();
    }
    $scope.addImageAnswer = function (answer, $event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "ANSWER_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("ANSWER_SELECT", function (ev, item){
        if(item){
          answer.image = item[0];
        }
        eventCalled();
      })
      $event.preventDefault();
    }
    $scope.removeImageQuestion = function (question, $event){
      delete question.image;
      $event.preventDefault();
    }
    $scope.removeImageResult = $scope.removeImageQuestion;
    
    $scope.removeImageAnswer = function (answer, $event){
      delete answer.image;
      $event.preventDefault();
    }
    $scope.removeAnswer = function (question, answer, $event){
      question.Answers = question.Answers.filter(function (item){
        if(item != answer){
          return item;
        }
      })
      $event.preventDefault();
    }
    $scope.removeResult = function(item, $event){
      $scope.content.Results  = $scope.content.Results.filter(function (result){
        if(item != result){
          return item;
        }
      })
      $event.preventDefault();
    }
    $scope.removeQuestion = function (Question, $event){
      if($event.preventDefault){ $event.preventDefault() }
      var $alertView = $modal.open({
        templateUrl: "views/tpl/alertModal.html",
        controller: "AlertCtrlModal",
        size: "sm",
        resolve: {
          initParams: function (){
            return {type: "YES/NO", message: "ARE_YOU_SURE_WANT_TO_REMOVE_QUESTION", title: "CAUTION!"}
          }
        },
      })
      $alertView.result.then(function (change){
        if(change == "YES"){
          $scope.content.Questions = $scope.content.Questions.filter(function (item){
            if(item != Question){
              return item;
            }
          })
        }
      })
      
      return false
    }
    
    $scope.changeSlug = function (){
      
      var modal = $modal.open({
          templateUrl: 'views/tpl/link.html',
          controller: 'SlugCtrlModal',
          backdrop: 'static',
          resolve: {'initParams': function (){return {'content': $scope.content}; }},
          size: 'lg'
        })
        
        modal.result.then(function (params){
          if(params.type == "OK") {
            $scope.content.slug = params.slug
            $scope.content.link = params.link
          }
        })
    }
    
    
    // Quizz Tools
    // Poll Tools
    $scope.addPoll = function ($event){
      if(!$scope.content.Polls){
        $scope.content.Polls = []
      }
      $scope.content.Polls.push({title: "", fullText: ""});
      $event.preventDefault()
    }
    $scope.removePoll = function(item, $event){
      $scope.content.Polls = $scope.content.Polls.filter(function (poll){
        if(item != poll){
          return item;
        }
      })
      $event.preventDefault();
    }
    $scope.addPollImage = function(poll, $event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "POLL_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("POLL_SELECT", function (ev, item){
        if(item){
          poll.File = item[0];
        }
        eventCalled()
      })
      
      $event.preventDefault()
    }
    $scope.removePollImage = function(item, $event){
      delete item.File;
      $event.preventDefault();
    }
    $scope.setOrderUp = function (item, $event){
      if($event.preventDefault){$event.preventDefault(); }
      
      $scope.content.Galery.splice((item.order-1), 0, $scope.content.Galery.splice(item.order, 1)[0]);
      
      for(var i in $scope.content.Galery){
        $scope.content.Galery[i]['order'] = parseInt(i);
      }  
      
      
    }
    $scope.setOrderDown = function (item, $event){
      if($event.preventDefault){$event.preventDefault(); }
      
      $scope.content.Galery.splice((item.order+1), 0, $scope.content.Galery.splice(item.order, 1)[0]);
      
      for(var i in $scope.content.Galery){
        $scope.content.Galery[i]['order'] = parseInt(i);
      }
      
      
    }
    
    $scope.insertTag = function (tag, type, $event){
      if($event){ $event.preventDefault(); }
      
       var modal = $modal.open({
          templateUrl: 'views/tpl/tag.html',
          controller: 'TagsCtrlModal',
          backdrop: 'static',
          resolve: {'initParams': function (){return {'name': tag, 'type': type}; }},
          size: 'lg'
        })
        $scope.browserOpen = true;
        modal.result.then(function (params){
          console.log(params)
          if(params.type == "OK"){
            $scope.formatTag(params.item);
          }
          //$scope.browserOpen = false;
        }, function (params){
            if(params.type == "OK"){
              $scope.formatTag(params.item);
            }
            //$scope.browserOpen = false;
        })
      
      
      
      /*
      var type = (!type)?"TAG":type;
      if(type == "PROFILE"){
        type = "ART_PROFILE";
      }
      
      Tags.create({name: tag, 'type': type}).$promise.then(function (data){
        $scope.formatTag(data, null, null);
      })
      */
      return false;
    }
    $scope.editTag = function(item, $event){
      
      var modal = $modal.open({
          templateUrl: 'views/tpl/tag.html',
          controller: 'TagsCtrlModal',
          backdrop: 'static',
          resolve: {'initParams': function (){return item; }},
          size: 'lg'
        })
        $scope.browserOpen = true;
        modal.result.then(function (params){
          item = params.item;
          //$scope.browserOpen = false;
        }, function (params){
            item = params.item;
            //$scope.browserOpen = false;
        })
        
        
      $event.preventDefault();
    }
    $scope.formatTag = function ($item, $model, $label){
      if(!$scope.content.Tags){
        $scope.content.Tags = [];
      }
      $scope.content.Tags.push($item);
      
      $scope.tagNew = "";
      $scope.tagNewProfile = "";
    }
    $scope.searchTag = function (tag, taxonomy){
      if(tag){
        if(tag.length >= 3){
          var params = {
            name: tag,
          }
          if(taxonomy){
            params.type = taxonomy;
          }
          return Tags.query2(params).$promise;
          /*
          Tags.query(params).$promise.then(function(data){
            console.log(data);
            return data;
          })
          */
        }
      }
      return 0;
    }
    
    $scope.changeQuizType = function (type, $event){
      $scope.content.quizType = type;
      $event.preventDefault();
    }

    $scope.generateLink = function (){
      var content = $scope.content;
      if(content.type && content.title && content.id){
        $scope.content.link = "/"+(content.type)+"/"+(content.title)+"-"+(content.id)+".html";
      }
    }
    // Helper methods
    $scope.deleteItem = function (item, event){
      event.preventDefault();
      event.stopPropagation();
      if(window.confirm("Tem certeza?")){
        Content.delete({id: item.id}).$promise.then(function (data){
          $route.reload();
        });
      }
      return false;
    }
    $scope.isContentType = function(type){
      return (type == $scope.content.type)
    }
    $scope.changeContentType = function (type, $event){
      $event.preventDefault();
      $event.stopPropagation();

      $scope.content.type = type;
      if($scope.content.type == "quiz"){
        $scope.content.quizType = "correct";
      }
    }
    $scope.removeTag = function (item){
      var index = $scope.content.Tags.indexOf(item);
      if(index > -1){
        $scope.content.Tags.splice(index, 1)
      }
    }
    $scope.addTag = function (item){
      $scope.content.tags.push(item)
    }
    $scope.contentSave = function ($event){
      //localStorageService.set("dreamzContentSaved", $scope.content);
      if($scope.content.id){
        $this.update();
      } else {
        $this.create();
      }
      $event.preventDefault();
      return false;
    }

    // Page Methods
    
    function adjustValues(){
      var obj = {};
      var allowedFields = ["id", "title", "intro", "slug","type","summary","fullText","writer","writerEmail","titleSEO","keywordsSEO","descriptionSEO","status","quizType","publishedAt","createdAt","updatedAt","deletedAt","SiteId"];
      
      for(var i in allowedFields){
        obj[(allowedFields[i])] = $scope.content[allowedFields[i]];
      }
      
      if($scope.content.extras){
        obj.extras_ids = [];
        for(var i in $scope.content.extras){
          obj.extras_ids.push($scope.content.extras[i]['id']);
        }
      }
      
      if($scope.content.thumbnail){
        obj.thumbnailID = $scope.content.thumbnail.id
        obj.thumbnailCredit = $scope.content.thumbnail.credits;
      }
      if($scope.content.Tags){
        obj.tags_ids = [];
        for(var z in $scope.content.Tags){
          var tag = $scope.content.Tags[z];
          obj.tags_ids.push(tag.id);
        }
      }
      if($scope.content.Editorials){
        obj.editorials_ids = [];
        for(var i in $scope.content.Editorials){
          obj.editorials_ids.push($scope.content.Editorials[i]['id']);
        }
      }
      return obj;
    }
    function adjustQuestion(gal){
      var g = {};
      if(gal.image){
        gal.FileId = gal.image.id;
      }
      var allowedFields = ["title", "fullText", "FileId"];
      for(var i in gal){
        if(allowedFields.indexOf(i) >= 0){
          g[i] = gal[i];
        }
      }
      return g;
    }
    function formatQuestions(galeries){
      var rets = [];
      for(var i in galeries){
        if(galeries[i]['title']){
          rets.push(adjustQuestion(galeries[i]));
        }
      }
      return rets;
    }
    function adjustResult(result){
      return adjustQuestion(result);
    }
    function formatResults(results){
      var rets = [];
      for(var i in results){
        if(results[i]['title']){
          rets.push(adjustResult(results[i]));
        }
      }
      return rets;
    }
    function adjustAnswer(answer){
      var g = {};
      if(answer.image){
        answer.FileId = answer.image.id;
      }
      var allowedFields = ["title", "fullText", "FileId"];
      for(var i in answer){
        if(allowedFields.indexOf(i) >= 0){
          g[i] = answer[i];
        }
      }
      return g;
    }
    function formatAnswers(answers){
      var rets = [];
      for(var i in answers){
        if(answers[i]['title']){
          rets.push(adjustAnswer(answers[i]));
        }
      }
      return rets;
    }
    
     
     
     
    
    function adjustPoll(poll){
      var g = {};
      if(poll.File){
        poll.FileId = poll.File.id;
      }
      var allowedFields = ["title", "fullText", "votes", "FileId"];
      for(var i in poll){
        if(allowedFields.indexOf(i) >= 0){
          g[i] = poll[i];
        }
      }
      return g;
    }
    
    function formatPolls(polls){
      var rets = [];
      for(var i in polls){
        if(polls[i]['title']){
          rets.push(adjustPoll(polls[i]));
        }
      }
      return rets;
    }
    function formatGallery(galeries){
      var rets = [];
      for(var i in galeries){
        rets.push(adjustGallery(galeries[i]));
      }
      return rets;
    }
    function adjustGallery(gal){
      var g = {};
      var allowedFields = ["title", "description", "order", "credits", "type", "FileId"];
      if(gal.id) {
        gal.FileId = gal.id;
        delete gal.id;
      }
      if(!gal.order){
        gal.order = 0;
      }
      for(var i in gal){
        if(allowedFields.indexOf(i) >= 0){
          g[i] = gal[i];
        }
      }
      g.type = (!g.type)?"GALLERY":"";
      return g;
    }
    this.index = function (){
      $scope.pagination = {itensPerPage: 10, currentPage: 1};
      $scope.model = Content
      $scope.controllerUrl = $route.current.params.site_id  + "/content"
      $scope.tableList = {columns: [{key: 'id', name: "ID"}, {key: 'title', name: 'TITLE'}, {key: 'type', name: 'contentType'}, {key: 'status', name: 'Status'}]};
      $scope.filters = {type: "", title: ""};

      $scope.$watch("qType", function (value){
        if(value){
          $scope.filters.type = value.id
        }
      })
      
      
    }
    this.new = function (){
      $scope.$watch("currentSite", function (val){
        if(val){
          $scope.content.SiteId = $scope.currentSite.id;
        }
        
      })
      //$scope.content = {};
       $scope.$watch("content.type", function (value, oldValue){
          if(value)
          {
            $scope.generateLink();
          }
        })
        $scope.$watch("content.title", function (value, oldValue){
          if(value)
          {
            $scope.generateLink();
          }
        })
        
    }
    
    function adjustImage(item){
      if(item.File){
        item.image = item.File;
      }
      return item
    }
    this.edit = function (){
      $scope.content = {};

      Content.edit($route.current.params).$promise.then(function (data){
        $scope.content = data;
        $scope.content.link = $scope.content.url;
        
        if($scope.content.type == "poll"){
          Content.poll({site_id: $scope.content.SiteId, id: $scope.content.id}).$promise.then(function (results){
            $scope.content.Polls = results;
          })
        }
        if($scope.content.type == "quiz"){
          Question.results({site_id: $scope.content.SiteId, content_id: $route.current.params.id}).$promise.then(function (results){

            $scope.content.Results = results;
            for(var i in $scope.content.Results){
              $scope.content.Results[i] = adjustImage($scope.content.Results[i]);
            }
          })
          Question.query({site_id: $scope.content.SiteId, content_id: $scope.content.id}).$promise.then(function (questions){
            $scope.content.Questions = questions;
            for(var i in $scope.content.Questions){
              
              $scope.content.Questions[i] = adjustImage($scope.content.Questions[i]);
              
              if($scope.content.Questions[i]['Answers']){
                for(var z in $scope.content.Questions[i]['Answers']){
                  $scope.content.Questions[i]['Answers'][z] = adjustImage($scope.content.Questions[i]['Answers'][z]);
                }
              }
            }
          })
        }
        
        if($scope.content.Site){
          $scope.content.link = (String($scope.content.Site.url + $scope.content.link)).replace(/\/\//g, "/");
        }
        
        if($scope.content.FilesContents){
          for(var i in $scope.content.FilesContents){
            var attach = $scope.content.FilesContents[i];
            if(attach['type'] == "THUMBNAIL"){
              $scope.content.thumbnail = attach.File;
              $scope.content.thumbnail.credits = attach.credits;
              //$scope.content.attachments.splice(i, 1);
            } else {
              if(!$scope.content.Galery){
                $scope.content.Galery = [];
              }
              
              var rel = attach.File;
              if(!attach.order){
                rel.order = 0;
              } else {
                rel.order = attach.order;
              }
              
              rel.title = attach.title;
              rel.description = attach.description;
              rel.credits = attach.credits;
              
              $scope.content.Galery.push(rel);
            }
          }
          delete $scope.content.FilesContents;
        }
      })
    }
    function checkVideos(){
      if($scope.content.Galery){
        for(var i in $scope.content.Galery){
          var rel = $scope.content.Galery[i];
          if(rel.fileType == "URL"){
            if(!$scope.content.Editorials){
              $scope.content.Editorials = [];
            }
            if($scope.vidEditorial){
              var hasVid = false;
              for(var i in $scope.content.Editorials){
                if($scope.content.Editorials[i] == $scope.vidEditorial){
                  hasVid = true;
                }
              }
              if(hasVid == false){
                $scope.content.Editorials.push($scope.vidEditorial);
              }
            }
            
          }
        }
      }
    }
    this.create = function (){

      checkVideos()
      //var val = $scope.content;
      var val = adjustValues();
      
      //return false;
      
      
      Content.create({site_id: val.SiteId}, val).$promise.then(function (data){
        //console.log(data);
        angular.extend($scope.content, data);
        if(!$scope.content.Galery && !$scope.content.Questions && !$scope.content.Results){
          $this.finishSave();
        }
        
        
        var arrPromises = []        
        if($scope.content.Galery){
          arrPromises.push(Content.gallery({site_id: $scope.content.SiteId, id: $scope.content.id}, formatGallery($scope.content.Galery)));
        }
        if($scope.content.Questions){
          //arrPromises.push(Content.gallery({site_id: $scope.content.SiteId, id: $scope.content.id}, formatGallery($scope.content.Questions)));
          Content.question({site_id: $scope.content.SiteId, id: $scope.content.id}, formatQuestions($scope.content.Questions)).$promise.then(function (result){
            for(var i in result){
              
              // Isnt a promise key
              if(result[i]['title']){
                var item = result[i];
                
                //   Extend old Itens 
                for(var z in $scope.content.Questions){
                  var item2 = $scope.content.Questions[z];
                  if(item.title == item2.title){
                    angular.extend(item2, item);
                    break
                  }
                }
              }
            }
            
            // Save Answers
            for(var o in $scope.content.Questions){
              var question = $scope.content.Questions[o];
              var answers = question.Answers;
              var sendAnswers = formatAnswers(answers);
              if(sendAnswers.length > 0){
                arrPromises.push(Question.createAnswers({site_id: $scope.content.SiteId, content_id: $scope.content.id, id: question.id}, sendAnswers))
              }
            }
          })
        }
        if($scope.content.Results){
          var results = formatResults($scope.content.Results);
          arrPromises.push(Question.createResults({site_id: $scope.content.SiteId, content_id: $scope.content.id}, results))
        }
        if($scope.content.Polls){
          var results = formatPolls($scope.content.Polls);
          arrPromises.push(Content.createPoll({site_id: $scope.content.SiteId, id: $scope.content.id}, results))
        }
      })
      
    }
    this.finishSave = function (){
      localStorageService.set("dreamzContentSaved", null);
      $location.path("/"+$scope.content.SiteId+"/content/"+$scope.content.id);
    }
    this.update = function (){
      var val = adjustValues();
            
      Content.update({id: val.id, site_id: val.SiteId}, val).$promise.then(function (data){
        //console.log(data);
        if(!$scope.content.Galery && !$scope.content.Questions && !$scope.content.Results){
          $this.finishSave();
        }
        
        
        var arrPromises = []        
        if($scope.content.Galery){
          arrPromises.push(Content.gallery({site_id: $scope.content.SiteId, id: $scope.content.id}, formatGallery($scope.content.Galery)));
        }
        if($scope.content.Questions){
          Content.question({site_id: $scope.content.SiteId, id: $scope.content.id}, formatQuestions($scope.content.Questions)).$promise.then(function (result){
            for(var i in result){
              
              // Isnt a promise key
              if(result[i]['title']){
                var item = result[i];
                
                //   Extend old Itens 
                for(var z in $scope.content.Questions){
                  var item2 = $scope.content.Questions[z];
                  if(item.title == item2.title){
                    angular.extend(item2, item);
                    break
                  }
                }
              }
            }
            
            // Save Answers
            for(var o in $scope.content.Questions){
              var question = $scope.content.Questions[o];
              var answers = question.Answers;
              var sendAnswers = formatAnswers(answers);
              if(sendAnswers.length > 0){
                arrPromises.push(Question.createAnswers({site_id: $scope.content.SiteId, content_id: $scope.content.id, id: question.id}, sendAnswers))
              }
            }

          })
        }
        if($scope.content.Results){
          var results = formatResults($scope.content.Results);
          console.log(results);
          
          arrPromises.push(Question.createResults({site_id: $scope.content.SiteId, content_id: $scope.content.id}, results))
        }
       if($scope.content.Polls){
          var results = formatPolls($scope.content.Polls);
          arrPromises.push(Content.createPoll({site_id: $scope.content.SiteId, id: $scope.content.id}, results))
        }
        
        /*
        if(arrPromises.length > 0){
          $q.all(arrPromises).then(function (result){
            Content.updateJson({site_id: $scope.content.SiteId, id: $scope.content.id}).$promise.then(function (){
              console.log("UPDATED JSON")
            })
          })
        }
        */

        
        
        
        //$location.path("/content/"+data.id);
      })
      
      //window.alert("RODOU2");
    }
    switch($route.current.action){
      case "NEW":
        this.new();
      break;
      case "EDIT":
        this.edit()
      break;
      default:
        this.index();
      break
    }

  })
  .controller('ContentCtrlModal', function ($scope, $route, Content, initParams, $modalInstance){
    
    $scope.selectItem = function ($item, $event){
      if($event.preventDefault){ $event.preventDefault() }
      $modalInstance.close({type: "OK", item: $item});
    }
    
    $scope.pagination = {itensPerPage: 10, currentPage: 1};
    $scope.model = Content
    $scope.tableView = {showEdit: false};
    $scope.controllerUrl = $route.current.params.site_id  + "/content"
    $scope.tableList = {columns: [{key: 'id', name: "ID"}, {key: 'title', name: 'TITLE'}, {key: 'type', name: 'contentType'}, {key: 'status', name: 'Status'}]};
    $scope.filters = {type: "", title: "", onlyPublished: true};

    $scope.$watch("qType", function (value){
      if(value){
        $scope.filters.type = value.id
      }
    })
    
    $scope.cancel = function (){
      $modalInstance.close({type: "canceled"});
    }
  })
