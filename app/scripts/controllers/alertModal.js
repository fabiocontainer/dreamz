'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('AlertCtrlModal', ['$scope', '$modalInstance', 'initParams', function ($scope, $modalInstance, initParams){
  $scope.message = initParams.message;
  $scope.title = initParams.title;
  
  $scope.ok = function (){
     $modalInstance.close("YES");
  }
  $scope.cancel = function (){
     $modalInstance.close("NO");
  }
}])