'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('FeaturedsCtrl', function ($scope, $route, $rootScope) {
    $rootScope.$watch("currentEdt", function (val){
      $scope.template = val.template;
    })
  });

