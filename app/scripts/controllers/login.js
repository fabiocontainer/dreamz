'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('LoginCtrl', function ($scope, $route, Token, API_Configs, localStorageService, $location, $rootScope) {
    $scope.classBody = 'login-body';
    this.index = function (){
      
    }
    this.logout = function (){
      localStorageService.remove("Dreamz_token");
      document.location = "/"
    }
    $scope.closeLight = function (){
      $location.path("/")
    }
    $scope.goLogin = function (){
      Token.login({"email": $scope.username, "pass": $scope.pass, 'appName': API_Configs.client_id, 'appSecret': API_Configs.secret}).$promise.then(function (data){
        
        if(data.token){
          localStorageService.set('Dreamz_token', data.token);
          $scope.closeLight();
        }
        
      })
    }
    // Routing
    switch($route.current.action){
      case "index":
        this.index();
      break
      case "logout":
        this.logout();
      break
    }    
  })
  .controller('LoginCtrlModal', ['$scope', 'API_Configs', 'Token', '$controller', '$modalInstance', 'localStorageService', '$route', '$rootScope', function ($scope, API_Configs, Token, $controller, $modalInstance, localStorageService, $route, $rootScope){
    $.extend(this, $controller('LoginCtrl', {'$scope': $scope}))
    $scope.closeLight = function (){
      $modalInstance.close();
      if(!$rootScope.currentSite){
        location.reload();
      }
    }
    
  }])
