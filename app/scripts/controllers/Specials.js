'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('SpecialsCtrl', function ($scope, $route, $rootScope, Special, Tags, $location) {
    var $this = this;
    $scope.saveItem = function ($event){

      if($scope.special){
        if($scope.special.id){
          $this.update();
        } else {
          $this.create();
        }
      }

      $event.preventDefault();
    }

   $scope.removeTag = function (item){
      delete $scope.special.Tag
      $scope.special.TagId = null;
    }
    $scope.formatTag = function ($item, $model, $label){

      $scope.special.Tag = $item;
      $scope.special.TagId = $item.id;

      $scope.tagNew = "";
    }

    $scope.searchTag = function (tag, taxonomy){
      if(tag){
        if(tag.length >= 3){
          var params = {
            name: tag,
          }
          if(taxonomy){
            params.type = taxonomy;
          }
          return Tags.query2(params).$promise;
        }
      }
      return 0;
    }



    $scope.addImage = function (itemToAdd, $event){

      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "SPECIAL_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("SPECIAL_SELECT", function (ev, item){
        if(item){
          $scope.special.File = item[0];
        }
        eventCalled();
      })

      $event.preventDefault();
    }
    $scope.removeImage = function (itemToAdd, $event){

      delete $scope.special.File;

      $event.preventDefault();
    }

    Tags.query2({perPage: 9999}).$promise.then(function (results){
      $scope.tags = results.rows;
    });

    this.index = function (){
      $scope.pagination = {itensPerPage: 10, currentPage: 1};
      $scope.model = Special
      $scope.controllerUrl = $route.current.params.site_id  + "/specials"
      $scope.tableList = {columns: [{key: 'id', name: "ID"}, {key: 'title', name: 'TITLE'}, {key: 'url', name: 'URL'}]};
      //$scope.filters = {"title":{"contains":""}, "type": ""}
    }
    this.new = function (){
      $scope.special = {};
    }

    this.edit = function (){
      $scope.special = {};
      Special.show($route.current.params).$promise.then(function (result){
        $scope.special = result;
      })
    }

    function formatSubmit(item){
      var obj = {};
      var allowedFields = ["id", "title", "url", "color", "titleSEO", "keywordsSEO", "descriptionSEO", "TagId"];
      if(item.File){
        obj.FileId = item.File.id;
      } else {
        obj.FileId = null;
      }
      for(var i in item){
        if(allowedFields.indexOf(i) >= 0){
          obj[i] = item[i];
        }
      }

      return obj;

    }

    $scope.deleteItem = function (item, event){
      event.preventDefault();
      event.stopPropagation();
      if(window.confirm("Tem certeza?")){
        Special.remove({site_id: $route.current.params.site_id, id: item.id}).$promise.then(function (data){
          $route.reload();
        });
      }
      return false;
    }

    this.create = function (){

      var obj = formatSubmit($scope.special);

      Special.create($route.current.params, obj).$promise.then(function(item){
        $location.path("/"+$rootScope.currentSite.id+"/specials/"+item.id);
      })
    }
    this.update = function (){
      var obj = formatSubmit($scope.special);

      Special.update($route.current.params, obj).$promise.then(function(item){

      })
    }


    switch($route.current.action){
      case "NEW":
        this.new();
      break;
      case "EDIT":
        this.edit()
      break;
      default:
        this.index();
      break
    }

  });
