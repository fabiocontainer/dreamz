'use strict';

/**
 * @ngdoc function
 * @name dreamzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dreamzApp
 */
angular.module('dreamzApp')
  .controller('TagsCtrl', function ($scope, $route, $rootScope, Tags, Files) {
    var $this = this;
    $scope.taxonomies = [{type: "TAG", name: "Tag"}, {type: "ART_PROFILE", name: "Perfil de Artista"}, {type: "ART_SERIES", name: "Perfil de Séries"}, {type: "ART_REALITIES", name: "Perfil de Realities"}]
    $scope.profileTypes = [{type: "CELEBRITIES", name: "Celebridade"}, {type: "TEEN", name: "Teen"}]
    $scope.content_types = [{id: "PROFILE", name: "PROFILE"}, {id: "TAG", name: "TAG"}]
    
    function allowedFields(input){
      var aFields = ["id", "job", "profileDate", "profileDeathDate", "profileType", "slug", "taxonomy", "title", "fileId"];
      var object = {};
      for(var i in aFields){
        var field = aFields[i];
        if(typeof(input[field]) != "undefined"){
          object[field] = input[field];
        }
      }
      return object;
    }
    
    
    $scope.saveContent = function (event){
      event.preventDefault();
      event.stopPropagation();
      
      if($scope.tag.File){
        $scope.tag.fileId = $scope.tag.File.id;
      }
      
      
      
      if($scope.tag.id){
        $this.update()
      } else {
        $this.create();
      }
    }
    $scope.deleteItem = function (item, event){
      event.preventDefault();
      event.stopPropagation();
      if(window.confirm("Tem certeza?")){
        Tags.remove({id: item.id}).$promise.then(function (data){
          $route.reload();
        });
      }
      return false;
    }
    
    $scope.tagRemoveImage = function ($event){
      delete $scope.tag.File;
      $event
    }
    $scope.tagAddImage = function($event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "TAG_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("TAG_SELECT", function (ev, item){
        if(item){
          $scope.tag.File = item[0];
        }
        eventCalled();
      })
      $event.preventDefault();
    }
    
    
    
    this.index = function(){
      $scope.pagination = {itensPerPage: 20, currentPage: 1};
      $scope.model = Tags
      $scope.controllerUrl = $route.current.params.site_id  + "/tags"
      $scope.tableList = {columns: [{key: 'id', name: "ID"}, {key: 'title', name: 'TITLE'}, {key: "taxonomy", name: 'Tipo'}, {key: "profileType", name: 'Tipo de Perfil'}]};
      $scope.filters = {type: "PROFILE", title: "", };

      $scope.$watch("qType", function (value){
        if(value){
          $scope.filters.type = value.id
        }
      })
      
    }
    this.new = function(){
      $scope.tag = {};
      
    }
    this.edit = function(){
      $scope.tag = {};
      
      Tags.edit({id: $route.current.params.id}).$promise.then(function (data){
        angular.extend($scope.tag, data)

        
        if($scope.tag.FileId){
          Files.show({id: $scope.tag.FileId}).$promise.then(function(file){
            $scope.tag.File = file;
          })
        }
      })
    }
    
    this.update = function (){
      var object = allowedFields($scope.tag);
      
      Tags.update({id: object.id}, object).$promise.then(function (data){
      })
      
    }
    this.create = function (){
      var object = allowedFields($scope.tag);
      
      Tags.create({}, object).$promise.then(function (data){
      })
    }
    
    switch($route.current.action){
      case "NEW":
        this.new();
      break;
      case "EDIT":
        this.edit()
      break;
      default:
        this.index();
      break
    }
  })
  .controller('TagsCtrlModal', function ($scope, $route, $rootScope, initParams, $modalInstance, Tags, Files) {
    
    $scope.taxonomies = [{type: "TAG", name: "Tag"}, {type: "ART_PROFILE", name: "Perfil de Artista"}, {type: "ART_SERIES", name: "Perfil de Séries"}, {type: "ART_REALITIES", name: "Perfil de Realities"}]
    $scope.profileTypes = [{type: "CELEBRITIES", name: "Celebridade"}, {type: "TEEN", name: "Teen"}]
    
    $scope.tag = {};
    
    angular.extend($scope.tag, initParams);
    
    if($scope.tag.FileId){
      Files.show({id: $scope.tag.FileId}).$promise.then(function(file){
        $scope.tag.File = file;
      })
    }
    
    $scope.tagRemoveImage = function ($event){
      delete $scope.tag.File;
      $event
    }
    $scope.tagAddImage = function($event){
      $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "TAG_SELECT", counts: 1});
      var eventCalled = $rootScope.$on("TAG_SELECT", function (ev, item){
        if(item){
          $scope.tag.File = item[0];
        }
        eventCalled();
      })
      $event.preventDefault();
    }
    $scope.ok = function(){
      if($scope.tag.File){
        $scope.tag.fileId = $scope.tag.File.id;
      }
      var test = null;
      if($scope.tag.id){
        test = Tags.update($scope.tag)
      } else {
        test = Tags.create($scope.tag)
      }
      test.$promise.then(function (data){
        $modalInstance.close({type: "OK", item: data});
        //$scope.formatTag(data, null, null);
      }).catch(function (){
        window.alert("ERROR while saving");
      })
      
      
      
      
    }
    $scope.cancel = function(){
      $modalInstance.close({type: "canceled"});
    }
  });

