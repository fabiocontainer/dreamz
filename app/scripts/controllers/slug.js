angular.module('dreamzApp')
  .controller('SlugCtrlModal', function ($scope, $route, $rootScope, initParams, $modalInstance, $filter) {
    $scope.content = initParams.content;
    
    $scope.date = new Date($scope.content.createdAt);
    
    $scope.slug = $scope.content.slug
    
    $scope.$watch("desiredTitle", function (value, oldValue){
      if(value != undefined){
        $scope.slug = value.toSlug()
      }
    })
    
    $scope.ok = function(){
      
      var type = $filter('translate')($scope.content.type);
      var date = $filter('date')($scope.date, "yyyy/MM/dd");
      var link = "/"+type+"/"+date+"/"+$scope.slug+"-"+$scope.content.id;
      
      $modalInstance.close({type: "OK", slug: $scope.slug, 'link': link});
    }
    $scope.cancel = function(){
      $modalInstance.close({type: "canceled"});
    }
  })