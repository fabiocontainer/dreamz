angular.module('dreamzApp')
  .factory('Tags', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/tags/:id", {id: "@id"}, {
      query: {
        method: 'GET',
        url: API_Configs.location + "/tags",
        isArray: false
      },
      query2: {
        method: 'GET',
        url: API_Configs.location + "/tags",
        isArray: true
      },
      edit: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/tags",
        isArray: false
      },
      update: {
        method: 'PUT',
        isArray: false
      },
      remove: {
        method: 'DELETE',
        isArray: false
      }
    }))
  })
