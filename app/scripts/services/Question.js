angular.module('dreamzApp')
  .factory('Question', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/:site_id/content/:content_id/questions/:id/answers", {site_id: "@site_id",content_id: "@content_id", id: "@id"}, {
      query: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/content/:content_id/questions",
        isArray: true
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content/:content_id/questions",
        isArray: false
      },
      createAnswers: {
        method: 'POST',
        //url: API_Configs.location + "/:site_id/content/:id/gallery",
        isArray: true
      },
      createResults: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content/:content_id/results",
        isArray: true
      },
      results: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/content/:content_id/results",
        isArray: true
      },
      
      delete: {
        method: 'DELETE'
      }
    }))
  })
