angular.module('dreamzApp')
  .factory('Special', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/:site_id/specials/:id", {site_id: "@site_id", id: "@id"}, {
      query: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/specials",
        isArray: false
      },
      show: {
        method: 'GET',
        isArray: false
      },
      update: {
        method: 'PUT',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/specials",
        isArray: false
      },
      remove: {
        method: 'DELETE',
        isArray: false
      }
    }))
  })
