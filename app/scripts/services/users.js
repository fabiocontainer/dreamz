'use strict';

/**
 * @ngdoc service
 * @name dreamzApp.Token
 * @description
 * # Editorials
 * Service in the dreamzApp.
 */
angular.module('dreamzApp')
  .factory('Users', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/users/:id", {id: "@id"}, {
      me: {
        method: 'GET',
        url: API_Configs.location + "/me",
        isArray: false
      },
      index: {
        method: 'GET',
        url: API_Configs.location + "/users",
        isArray: true
      },
      show: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/users",
        isArray: false
      },
      update: {
        method: 'PATCH',
        isArray: false
      },
      destroy: {
        method: 'DELETE',
        isArray: false
      },
    }))
  });
