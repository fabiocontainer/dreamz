'use strict';

/**
 * @ngdoc service
 * @name dreamzApp.Token
 * @description
 * # Editorials
 * Service in the dreamzApp.
 */
angular.module('dreamzApp')
  .factory('Analytics', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/:siteid/analytics/pageviews", {siteid: "@siteid"}, {
      pageviews: {
        method: 'GET',
        isArray: false
      },
      keywords: {
        method: 'GET',
        url: API_Configs.location + "/:siteid/analytics/keywords",
        isArray: false
      },
      visits: {
        method: 'GET',
        url: API_Configs.location + "/:siteid/analytics/visits",
        isArray: false
      }
    }))
  });
