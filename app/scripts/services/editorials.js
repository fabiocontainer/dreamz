'use strict';

/**
 * @ngdoc service
 * @name dreamzApp.Token
 * @description
 * # Editorials
 * Service in the dreamzApp.
 */
angular.module('dreamzApp')
  .factory('Editorials', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/:site/editorials/:id", {id: "@id"}, {
      sites: {
        method: 'GET',
        url: API_Configs.location + "/sites",
        isArray: true
      },
      editorials: {
        method: 'GET',
        url: API_Configs.location + "/:site/editorials",
        params: {'site': '@site'},
        isArray: true
      },
      homes: {
        method: 'GET',
        url: API_Configs.location + "/:site/homes",
        params: {'site': '@site'},
        isArray: true
      },
      homeTemplate: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/editorials/:editorial_id/template",
        params: {'site_id': '@site_id', 'editorial_id': '@editorial_id'},
        isArray: false
      },
      featureds: {
        method: 'GET',
        url: API_Configs.location + "/editorials/:id/featureds",
        isArray: false
      },
      featuredsLists: {
        method: 'GET',
        url: API_Configs.location + "/editorials/:id/featureds/starts",
        isArray: false
      },
      featuredsBulk: {
        method: 'POST',
        url: API_Configs.location + "/editorials/:id/featureds/bulk",
        isArray: true
      },
      index: {
        method: 'GET',
        url: API_Configs.location + "/editorials",
        isArray: true
      },
      show: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/editorials",
        isArray: false
      },
      update: {
        method: 'PATCH',
        isArray: false
      },
      destroy: {
        method: 'DELETE',
        isArray: false
      },
    }))
  });
