'use strict';

/**
 * @ngdoc service
 * @name dreamzApp.Token
 * @description
 * # Token
 * Service in the dreamzApp.
 */
angular.module('dreamzApp')
  .factory('Token', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/apps/", {}, {
      login: {
        method: 'POST',
        url: API_Configs.location + "/oauth/token",
        cache: false,
        isArray: false
      }
    }))
  });
