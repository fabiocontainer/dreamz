angular.module('dreamzApp')
  .factory('Files', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/files/:id", {id: "@id"}, {
      query: {
        method: 'GET',
        url: API_Configs.location + "/files",
        isArray: false
      },
      count: {
        method: 'GET',
        url: API_Configs.location + "/files/count",
        isArray: false
      },
      edit: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/files",
        isArray: true
      },
      show: {
        method: 'GET',
        isArray: false
      },
      delete: {
        method: 'DELETE'
      }
    }))
  })
