angular.module('dreamzApp')
  .factory('Content', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/:site_id/content/:id", {site_id: "@site_id",id: "@id"}, {
      query: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/content",
        isArray: false
      },
      query2: {
        method: 'GET',
        url: API_Configs.location + "/:site_id/content",
        isArray: true
      },
      edit: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content",
        isArray: false
      },
      gallery: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content/:id/gallery",
        isArray: true
      },
      question: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content/:id/questions",
        isArray: true
      },
      answers: {
        method: 'POST',
        url: API_Configs.location + "/:site_id/content/:content_id/questions/:id/answers",
        params: {content_id: "@content_id", site_id: "@site_id", id: "@id"},
        isArray: true
      },
      updateJson: {
        method: 'PUT',
        url: API_Configs.location + "/:site_id/content/:id/updateJson"
      },
      poll: {
        method: "GET",
        url: API_Configs.location + "/:site_id/content/:id/polls",
        isArray: true
      },
      createPoll: {
        method: "POST",
        url: API_Configs.location + "/:site_id/content/:id/polls",
        isArray: true
      },
      update: {
        method: 'PUT',
        isArray: false
      },
      delete: {
        method: 'DELETE'
      }
    }))
  })
