'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .filter("gadate", function (){
    return function (input){
      var reg = new RegExp("([0-9]{4})([0-9]{2})([0-9]{2})");
      var matches = reg.exec(input)
      if(matches){
        return matches[3]+"/"+matches[2]+"/"+matches[1];
      }
      return "";
    }
  })