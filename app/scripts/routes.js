var routes = [
  {
    route: '/:site_id/specials/new',
    templateUrl: 'views/specials-edit.html',
    controller: 'SpecialsCtrl',
    action: 'NEW'
  },
  {
    route: '/:site_id/specials/:id',
    templateUrl: 'views/specials-edit.html',
    controller: 'SpecialsCtrl',
    action: 'EDIT'
  },
  {
    route: '/:site_id/specials',
    templateUrl: 'views/specials.html',
    controller: 'SpecialsCtrl',
    action: 'index'
  },
  {
    route: '/:editorial_id/home/edit',
    templateUrl: 'views/featureds.html',
    controller: 'FeaturedsCtrl',
    action: 'edit'
  },
  {
    route: '/',
    templateUrl: 'views/main.html',
    controller: 'MainCtrl',
    action: 'index'
  },
  {
    route: '/login',
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl',
    action: 'index'
  },
  {
    route: '/logout',
    templateUrl: 'views/logout.html',
    controller: 'LoginCtrl',
    action: 'logout'
  },
  {
    route: '/:site_id/content',
    templateUrl: 'views/content/index.html',
    controller: 'ContentCtrl',
    action: 'INDEX'
  },
  {
    route: '/:site_id/content/new',
    templateUrl: 'views/content/new.html',
    controller: 'ContentCtrl',
    action: 'NEW'
  },
  {
    route: '/:site_id/content/:id',
    templateUrl: 'views/content/new.html',
    controller: 'ContentCtrl',
    action: 'EDIT'
  },
  {
    route: '/:site_id/tags/new',
    templateUrl: 'views/tags/new.html',
    controller: 'TagsCtrl',
    action: 'NEW'
  },
  {
    route: '/:site_id/tags/:id',
    templateUrl: 'views/tags/new.html',
    controller: 'TagsCtrl',
    action: 'EDIT'
  },
  {
    route: '/:site_id/tags',
    templateUrl: 'views/tags/index.html',
    controller: 'TagsCtrl',
    action: 'INDEX'
  }
]