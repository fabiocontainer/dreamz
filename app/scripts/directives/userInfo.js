'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('userInfo', function (Users, $rootScope, localStorageService) {
    return {
      templateUrl: 'views/tpl/userInfo.html',
      restrict: 'E',
      link: function(scope, element, attrs){
        
        $rootScope.userCan = function (permission){
          var perms = localStorageService.get('Dreamz_Permissions');
          if(perms && perms[permission]){
            return true;
          }
          return false;
        }
        scope.me = {}
        Users.me().$promise.then(function (results){
          scope.me = results
          
          if(results.Groups){
            var permissions = {};
            for(var i in results.Groups){
              var group = results.Groups[i];
              
              if(group.Permissions){
                for(var z in group.Permissions){
                  permissions[group.Permissions[z]["role"]] = true;
                }
              }
            }
          }
          
          localStorageService.set('Dreamz_Permissions', permissions);
          //$rootScope.$broadCast("USER_INFO", results);
        })
      }
    }
  })