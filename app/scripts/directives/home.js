'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */

angular.module('dreamzApp')
  .directive('home', function ($compile, $rootScope, localStorageService, Editorials) {
    return {
      restrict: 'E',
      link: function ($scope, element, attr){
        $scope.$watch("template", function(val){
          element.html($compile("<template-"+val+"></template-"+val+">")($scope));

          //var confs = localStorageService.get("HOME_EDITORIAL");
          //$scope.home = confs.editorials;
          //$scope.homeConfigs = confs.configs;



        })
        $scope.isEdtOpen = false;
        $scope.homeConfigs = {publishAt: (new Date())};

        $scope.loadedHome = false;
        $scope.currentEditionDate = null;
        $scope.currentDates = [];

        $scope.openCloseEdt = function($event){
          $scope.isEdtOpen = ($scope.isEdtOpen)?false:true;
          $event.preventDefault();
        }
        $scope.setStyle = function ($item, $event){
          var response = window.prompt("Qual é o estilo desejado? \n(celebridades | estilo | series | teen | realities | videos)");
          if(response)
          {
            switch(response){
              case "celebridades":
                $item.style = "celebridades";
              break;
              case "estilo":
                $item.style = "estilo";
              break;
              case "series":
                $item.style = "series";
              break;
              case "teen":
                $item.style = "teen";
              break;
              case "realities":
                $item.style = "realities";
              break;
              case "videos":
                $item.style = "videos";
              break;
              default:
                 return $scope.setStyle($item, $event);
              break;
            }
          }
          $event.preventDefault();
        }
        $scope.setLink = function($item, $event){
          $event.preventDefault();
          $item.link =  prompt("Link:", $item.link);
        }
        $scope.setPhoto = function($item, $event){
          $event.preventDefault();
          //$item.link =  prompt("Link:", $item.link);
          $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "FEATURED_SELECT", counts: 1, contentType: 'images'});
          var eventCalled = $rootScope.$on("FEATURED_SELECT",  function (ev, item){
            if(item[0]){
                $item.image = item[0];
            }
            eventCalled();
          })
        }

        $scope.publishHome = function ($event){

          for(var i in $scope.currentDates.data){
            var d = moment($scope.currentDates.data[i]['startDate']);
            var cDate = moment($scope.homeConfigs.publishAt);

            var dStr = d.format();
            var cStr = cDate.format();


            if(dStr == cStr){
              $scope.homeConfigs.publishAt = cDate.add(1, 's')
              break;
            }
          }


          var date = $scope.homeConfigs.publishAt;
          for(var i in $scope.home){
            if(i != "publishAt"){
              for(var z in $scope.home[i]){
                $scope.home[i][z]["type"] = i;
                $scope.home[i][z]["startDate"] = date;
              }
            }
          }

          function formatFeatured(featured){
            var obj = {};
            if(featured.title){
              obj.title = featured.title;
            }
            if(featured.type){
              obj.type = featured.type;
            }
            if(featured.link){
              obj.link = featured.link;
            }
            if(featured.style){
              obj.style = featured.style;
            }
            if(featured.intro){
              obj.intro = featured.intro;
            }
            if(featured.fullText){
              obj.fullText = featured.fullText;
            }
            if(featured.startDate){
              obj.startDate = $scope.homeConfigs.publishAt;
            }
            /*
            if(featured.endDate){
              obj.endDate = featured.endDate;
            }
            */
            if(featured.image && featured.image.id){
              obj.fileIds = [featured.image.id];
            }

            return obj;
          }

          localStorageService.set("HOME_EDITORIAL", {configs: $scope.homeConfigs, editorials: $scope.home});
          var sendList = []
          var counts = 0;
          var lastCount = 0;
          for(var i in $scope.home){
            for(var z in $scope.home[i])
            {
              sendList.push($scope.home[i][z]);
            }
          }

          var filesToSend = 100;

          var reqCounts = Math.ceil(sendList.length / filesToSend);



          function saveContents($index){



            var i = $index;
            var sendNow = []
            for(var z = i * filesToSend; z < ((i * filesToSend)+filesToSend); z++){
              if(sendList[z]){
                sendNow.push(formatFeatured(sendList[z]));
              }
            }
            //console.log(sendNow);
            Editorials.featuredsBulk({id: $rootScope.currentEdt.id}, sendNow).$promise.then(function (data){
              if(($index+1) < reqCounts){
                saveContents(($index+1));
              }
            })
          }
          saveContents(0);

          $event.preventDefault();
        }

      }
    }
  })
  .directive("templateHome", function ($rootScope, API_Configs, Editorials){
    return {
      templateUrl: "views/templates/home/index.html",
      restrict: 'E',
      link: function ($scope, element, attr){
        var $link = angular.element("<link rel=\"stylesheet\" href=\"styles/templates/home/style.css\" />")
        angular.element('head').append($link);

        $scope.home = {};

        function defaultWidget(){
          return {
            title: "Lorem Ipsum dolor sit amen",
            style: "celebridades",
            link: "http://www.estrelando.com.br/",
            fullText: "Lorem Ipsum dolor sit amen",
            image: {
              thumbnails: {
                "300x300": '/uploads/icn-estrelando.png',
                '100x100': '/uploads/icn-estrelando.png',
                '162x115': '/uploads/icn-estrelando.png',
                '600x360': '/uploads/icn-estrelando.png',
                '450x270': '/uploads/icn-estrelando.png',
                '465x220': '/uploads/icn-estrelando.png',
                '296x180': '/uploads/icn-estrelando.png',
                '230x450': '/uploads/icn-estrelando.png',
                '230x515': '/uploads/icn-estrelando.png',
                '230x220': '/uploads/icn-estrelando.png',
                '539x310': '/uploads/icn-estrelando.png',
                '194x176': '/uploads/icn-estrelando.png',
                '732x398': '/uploads/icn-estrelando.png',
                '335x220': '/uploads/icn-estrelando.png',
                '335x280': '/uploads/icn-estrelando.png',
                '540x515': '/uploads/icn-estrelando.png',
                '620x100': '/uploads/icn-estrelando.png'
              }
            }
          };
        }
        $scope.configs = {
          "DTS": {counts: 5},
          "MD": {counts: 5},
          "TI": {counts: 1},
          "TI2": {counts: 1},
          "TI3": {counts: 1},
          "TI4": {counts: 1},
          "TI5": {counts: 1},
          "TI6": {counts: 1},
          "TI7": {counts: 1},
          "TI8": {counts: 1},
          "DT1": {counts: 6},
          "PR1": {counts: 5},
          "DT2": {counts: 4},
          "DT3": {counts: 2},
          "DT4": {counts: 5},
          "DT5": {counts: 5},
          "NV": {counts: 1},
          "NV2": {counts: 5}
        }





        for(var i in $scope.configs){
          if(!$scope.home[i])
          {
            $scope.home[i] = [];
          }
          var item = $scope.home[i];
          for(var z = (item.length); z < $scope.configs[i]['counts']; z++){

            item.push(defaultWidget());
          }
        }

        $rootScope.$watch("currentEdt", function (val){
          if(val){

            Editorials.featuredsLists({id: $rootScope.currentEdt.id}).$promise.then(function (data){
              $scope.currentDates = data;
              $scope.currentEditionDate = data.data[0]['startDate'];
              $scope.homeConfigs.publishAt = data.data[0]['startDate'];
            })

            $scope.$watch("currentEditionDate", function (val, oldVal){
              if(val){
                $scope.loadedHome = false;
                Editorials.featureds({id: $rootScope.currentEdt.id, startDate: val}).$promise.then(function (data){
                  $scope.loadedHome = true;
                  /*
                  for(var i in $scope.home){
                    $scope.home[i] = [];
                  }
                  */
                  for(var i in data.data){
                    var item = data.data[i];
                    $scope.home[item.type] = [];
                  }

                  for(var i in data.data){
                    var item = data.data[i];
                    if(item.Files && item.Files[0]){
                      item.image = item.Files[0];
                      delete item.Files[0];
                    }



                    if($scope.configs[item.type]){
                      var target = $scope.home[item.type]
                      var config = $scope.configs[item.type]
                      if((target.length+1) <= config.counts){
                        target.push(item);
                      }

                    }


                    //console.log(data.data[i]);
                  }
                })
              }

            })

          }
        })

        $scope.imagePrefix = API_Configs.location;






      }
    }
  })
