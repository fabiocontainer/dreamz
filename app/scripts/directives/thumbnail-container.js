'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('thumbnailContainer', function (Users, $rootScope, $modal, API_Configs) {
    return {
      templateUrl: 'views/tpl/thumbnails.html',
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.showImage = false;
        
        function getMedia(ev, item){
          if(item[0]){
            scope.content.thumbnail = item[0];
            scope.content.thumbnail.url = API_Configs.location + scope.content.thumbnail.url;
            
          }
          $rootScope.$off("THUMBNAIL_SELECT", getMedia);
        }
        
        scope.$watch("content.thumbnail", function (val, oldValue){
          if(val && val.url && !oldValue){
            if(scope.content.thumbnail.url.indexOf(API_Configs.location) < 0){
              scope.content.thumbnail.url = API_Configs.location + scope.content.thumbnail.url;
            }
          }
          if(val && val.url && oldValue && val.url != oldValue.url){
            if(scope.content.thumbnail.url.indexOf(API_Configs.location) < 0){
              scope.content.thumbnail.url = API_Configs.location + scope.content.thumbnail.url;
            }
          }
          
          if(val && val.url){
            scope.content.thumbnail.url = scope.content.thumbnail.url.replace(API_Configs.location+API_Configs.location, API_Configs.location);
          }
        })
        
        scope.removeThumbnail = function (e){
          if(e.preventDefault){ e.preventDefault(); }
          scope.showImage = false;
          scope.content.thumbnail = {};
          
          delete scope.content.thumbnail;
          
          return false;
        }
        scope.changeThumbnail = function (){
          $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "THUMBNAIL_SELECT", counts: 1, contentType: 'images'});
          
          $rootScope.$on("THUMBNAIL_SELECT", getMedia)
        }
        scope.$watch("content.thumbnail", function (val, newVal){
          if(typeof(val) != 'undefined' && typeof(val.url) != 'undefined'){
              scope.showImage = true;
          }
        })
      }
    }
  })