'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('fileBrowserItem', function (Users, $rootScope, $modal, API_Configs) {
    return {
      template: '<a href="#/{{item.id}}"><image-api format="300x300"></image-api></a>',
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.apiURI = API_Configs.location;
        //scope.itemToShow = (scope.item.thumbnails && scope.item.thumbnails["100x100"])?scope.item.thumbnails["100x100"]:scope.item.url;
        
        
       
        element.click(function (e){
          if(e.preventDefault){ e.preventDefault(); }
          scope.addSelectedItem(scope.item);
          return false;
        })
      }
    }
  })