'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('editorialList', function (Editorials, $rootScope) {
    return {
      templateUrl: 'views/tpl/editoriallist.html',
      restrict: 'E',
      scope: {},
      link: function(scope, element, attrs){
        scope.editorials = []
        scope.itemSelected = {}
        
        scope.changeEditorial = function (item, event){
          if(event){
            event.preventDefault();
            event.stopPropagation();
          }
          
          
          scope.itemSelected = item;
          $rootScope.$broadcast("CHANGED_EDITORIAL", item);
        }
        
        scope.$on("CHANGED_SITE", function (event, item){
          scope.editorials.push({id: item.id, name: 'Home'})
          Editorials.homes({site: item.id}).$promise.then(function (result){
            for(var i = 0; i<result.length; i++){
              scope.editorials.push(result[i])
            }
            if(scope.editorials[0])
            {
              scope.changeEditorial(scope.editorials[0])
            }
          })
        })
      }
    }
  })