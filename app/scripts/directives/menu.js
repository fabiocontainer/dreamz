'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('menu', function (Editorials, $rootScope) {
    return {
      templateUrl: 'views/tpl/menu.html',
      restrict: 'E',
      link: function (scope, element, attrs){
        scope.currentEdt = {}
        scope.showFeaturedsButton = false;
        scope.$on("CHANGED_EDITORIAL", function (event, values){
          Editorials.show({site: scope.currentSite.id, id: values.id}).$promise.then(function (result){
            scope.currentEdt = result
            $rootScope.currentEdt = scope.currentEdt
            
            if(scope.currentEdt.template){
              scope.showFeaturedsButton = true;
            } else {
              scope.showFeaturedsButton = false;
            }
          })
        }) 
      }
    };
  });
