'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('sitesList', function ($modal, Editorials, $rootScope) {
    return {
      templateUrl: 'views/tpl/siteslist.html',
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.sites = [];
        scope.itemSelected = {};
        
        scope.changeSite = function(item, event){
          if(event){
            event.preventDefault();
            event.stopPropagation();
          }
          
          $rootScope.currentSite = item;
          scope.itemSelected = item;
          $rootScope.$broadcast("CHANGED_SITE", item)
        }
        
        Editorials.sites().$promise.then(function (result){
          scope.sites = result
          scope.changeSite(result[0])
        })
      }
    }
  })