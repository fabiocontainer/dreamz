'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('localized', function ($translate) {
    return {
      restrict: 'A',
      
      link: function(scope, element, attrs){
        var title = attrs.localized;
        $translate(title).then(function (translation){
          element.attr("title", translation)
        })
        
      }
    }
  })