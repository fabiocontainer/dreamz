'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('analyticsPageviews', function (Analytics, $rootScope) {
    return {
      templateUrl: 'views/blocks/analytics/pageviews.html',
      restrict: 'E',
      scope: {},
      link: function(scope, element, attrs){
        scope.rows = [];
        scope.$on("CHANGED_SITE", function (event, site){
          Analytics.pageviews({siteid: site.id}).$promise.then(function (data){
            scope.rows = data.rows
          })
        })
      }
    }
  })