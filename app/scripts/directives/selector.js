'use strict';

/**
 * @ngdoc directive
 * @name AardvarkApp.directive:chosen
 * @description
 * # selector
 */
angular.module('dreamzApp')
  .directive('selector', function ($timeout) {
    return {
			scope: false,
			link: function (scope, element, attrs){
				if(attrs.ngModel)
				{
					var item = String(attrs.ngModel);
					var models = item.split('.');
					var prop = models[1];

					scope.$watchCollection(item, function(newValue, oldValue){
          	if(newValue && newValue != oldValue){
              element.val(newValue)
						}
					})
				}
			}
    };
  });
