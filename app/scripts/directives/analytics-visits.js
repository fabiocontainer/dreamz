'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('analyticsVisits', function (Analytics, $rootScope) {
    return {
      templateUrl: 'views/blocks/analytics/visits.html',
      restrict: 'E',
      scope: {},
      link: function(scope, element, attrs){
        scope.rows = [];
        scope.$on("CHANGED_SITE", function (event, site){
          Analytics.visits({siteid: site.id}).$promise.then(function (data){
            scope.rows = data.rows
          })
        })
      }
    }
  })
  .directive('analyticsVisits2', function (Analytics, $rootScope, $filter) {
    return {
      templateUrl: 'views/blocks/analytics/visits2.html',
      restrict: 'E',
      scope: {},
      link: function(scope, element, attrs){
        scope.rows = [];
        
        scope.$on("CHANGED_SITE", function (event, site){
          Analytics.visits({siteid: site.id}).$promise.then(function (data){
            //scope.rows = data.rows
            
            
            var filter = $filter("gadate");
            
            for (var i in data.rows) {
              data.rows[i][0] = (filter(data.rows[i][0]))
              data.rows[i][1] = Number(data.rows[i][1])
            }
            var startDate = data.rows[0][0];
            var endDate = data.rows[(data.rows.length - 1)][0];
            
                        
            $("#chartVisits", element).highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Visitas'
                    },
                    subtitle: {
                        text: 'From '+startDate+' until '+endDate
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Visits'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Visits: <b>{point.y:.0f}</b>'
                    },
                    series: [{
                        name: '',
                        'data': data.rows,
                        dataLabels: {
                            enabled: false,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
                
                
          })
        })
      }
    }
  })