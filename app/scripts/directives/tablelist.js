'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('tablelist', function ($rootScope, $route, $compile) {
    return {
      templateUrl: 'views/tpl/tablelist.html',
      restrict: 'E',
      link: function ($scope, element, attr){
      $scope.$watch("pagination.currentPage", function (value, old){
        if(value != old){
          $scope.callPage();
        }
      })
      $scope.pagination.totalItens = 0;

      $scope.$watch("pagination.totalPages", function (value, old){
        if(value){
          if(value != old){
            $scope.pagination.startPages = Math.max(1, ($scope.pagination.currentPage-3))
            $scope.pagination.endPages = Math.min($scope.pagination.totalPages, ($scope.pagination.currentPage+3))
          }
        }
      })
      $scope.$watch("pagination.itensPerPage", function (value, old){
        if(value != old){
          $scope.pagination.totalPages = Math.ceil($scope.pagination.totalItens/$scope.pagination.itensPerPage);
          
          
          if(isNaN($scope.pagination.totalPages)){
            $scope.pagination.totalPages = 1
          }

          $scope.callPage();
        }
      })
      $scope.$on("RELOAD_PAGE", function (){
        $scope.callPage();
      })
      /*
      $scope.paginationRules = function (){
        $scope.model.count().$promise.then(function (data){
          $scope.pagination.totalItens = data.count;
          $scope.pagination.totalPages = Math.ceil($scope.pagination.totalItens/$scope.pagination.itensPerPage);
          if(isNaN($scope.pagination.totalPages)){
            $scope.pagination.totalPages = 1
          }
        })
      }
      */
      $scope.filterContent = function($event){
        $event.preventDefault();
        if($scope.pagination.currentPage > 1){
          $scope.pagination.currentPage = 1;
        } else {
          $scope.callPage();
        }
        
        
        //$scope.callPage();
      }
      $scope.goPage = function (page, event){
        $scope.itens = [];
        $scope.pagination.currentPage = page;
        $scope.callPage(event);
      }
      $scope.callPage = function (){
        var params = $route.current.params;
        $scope.loading = true;
        angular.extend(params, {perPage: $scope.pagination.itensPerPage, page: ($scope.pagination.currentPage-1)});
        
        var filters = angular.extend({}, $scope.filters);
        if(filters.type){
          filters.type = $scope.filters.type.name;
        }
        
        angular.extend(params, filters);
        
        $scope.model.query(params).$promise.then(function (data){
          $scope.pagination.totalItens = data.count;
          $scope.itens = data.rows;
          $scope.loading = false;
          if(data.rows[0]){
            $scope.columns = $scope.getColumns(data.rows[0])
          }
          
        })
      }
      $scope.$watch("pagination.totalItens", function(newVal){
        if(newVal){
          $scope.pagination.totalPages = Math.ceil($scope.pagination.totalItens/$scope.pagination.itensPerPage);
        }
      })
      $scope.getColumns = function (data){
        if($scope.tableList && $scope.tableList.columns){
          return $scope.tableList.columns;
        } else {
          if(data){
            var cols = [];
            for(var i in data){
              cols.push({key: i, name: i});
            }
            return cols;
          }
        }
      }
      $scope.goNext = function (event){
        event.preventDefault();
        event.stopPropagation();
        $scope.pagination.currentPage++;
      }
      $scope.goPrev = function (event){
        event.preventDefault();
        event.stopPropagation();
        $scope.pagination.currentPage--;
      }
      $scope.goFirstPage = function (event){
        event.preventDefault();
        event.stopPropagation();
        $scope.pagination.currentPage = 1;
      }
      $scope.goLastPage = function (event){
        event.preventDefault();
        event.stopPropagation();
        $scope.pagination.currentPage = $scope.pagination.totalPages;
      }
      //$scope.paginationRules();
      $scope.callPage();
    }
  }
  })
