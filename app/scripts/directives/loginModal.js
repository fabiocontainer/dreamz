'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('loginModal', function ($modal) {
    return {
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.alreadyOpen = false;
        scope.$on("HTTP_FORBIDDEN", function (){
          // Modal
          if(!scope.alreadyOpen) {
            var modal = $modal.open({
              templateUrl: 'views/login.html',
              controller: 'LoginCtrlModal',
              backdrop: 'static'
            })
            scope.alreadyOpen = true;
            modal.result.then(function (){
              scope.alreadyOpen = false;
            })
          }
        })
      }
    }
  })