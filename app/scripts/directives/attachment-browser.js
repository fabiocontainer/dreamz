'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('attachmentBrowser', function ($rootScope, $modal) {
    return {
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.browserOpen = false;
        $rootScope.$on("ATTACHMENT_ADD", function (event, params){
            
            if(!scope.browserOpen) {
              var modal = $modal.open({
                templateUrl: 'views/tpl/browser.html',
                controller: 'AttachmentsCtrlModal',
                backdrop: 'static',
                resolve: {'initParams': function (){return params; }},
                size: 'lg'
              })
              scope.browserOpen = true;
              modal.result.then(function (){
                scope.browserOpen = false;
              }, function (){
                  scope.browserOpen = false;
              })
            }
        })
      }
    }
  })
  .directive('imaginateProgressBar', function ($rootScope){
    return {
      template: '<div ng-if="item.isUploading" class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">{{item.progress}}%</div></div><strong ng-if="item.isUploaded" translate>UPLOAD_DONE</strong>',
      restrict: "E",
      link: function (scope, element, attrs){
        scope.$watch("item", function (val){
          if(val && val.isUploading){
            angular.element(".progress-bar", element).attr("aria-valuenow", val.progress);
          }
        })
      }
    }
  })
  .directive('imageApi', function (API_Configs){
    return {
      template: '<img ng-if="item.fileType == \'FILE\'" ng-src="{{apiURI}}{{itemToShow}}" data-id="{{idFile}}" /><img ng-if="item.fileType == \'URL\'" ng-src="{{itemToShow}}" title="{{item.url}}" data-id="{{idFile}}" />',
      restrict: "E",
      link: function (scope, element, attrs){
        if(attrs.prop){
          
        }
        if(scope.item && scope.item.fileType == "FILE"){
          scope.apiURI = API_Configs.location;
          scope.idFile =  scope.item.id;
          var format = (attrs.format)?attrs.format:"100x100";
          scope.itemToShow = (scope.item.thumbnails && scope.item.thumbnails[format])?scope.item.thumbnails[format]:scope.item.url;

          if(scope.itemToShow && scope.itemToShow.indexOf(scope.apiURI) >= 0){
            scope.apiURI = "";
          }
          if(scope.itemToShow && scope.itemToShow.indexOf(scope.apiURI+scope.apiURI) >= 0){
            scope.itemToShow.replace(scope.apiURI+scope.apiURI, scope.apiURI);
          }
          if(scope.item.fileType != 'FILE'){
            scope.itemToShow  = "/images/videos.png";
          }
        }
        if(scope.item && scope.item.fileType != "FILE"){
          scope.apiURI = API_Configs.location;
          scope.idFile =  scope.item.id;
          scope.itemToShow  = "/images/video.png";
          
          if(/(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(scope.item.url)){
            var matches = /(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(scope.item.url);
            scope.itemToShow  = "https://i.ytimg.com/vi/"+matches[2]+"/hqdefault.jpg";
          }
          
          
          
        }        
      }
    }
  })
  .directive('imageApi2', function (API_Configs){
    return {
      template: '<img ng-if="mediaType == \'FILE\'" ng-src="{{apiURI}}{{itemToShow}}" data-id="{{idFile}}" />',
      restrict: "E",
      scope: {
        item: "=item"
      },
      link: function (scope, element, attrs){

        if(scope.item){
          scope.apiURI = API_Configs.location;
          var format = (attrs.format)?attrs.format:"100x100";
          scope.idFile =  scope.item.id;
          scope.itemToShow = (scope.item.thumbnails && scope.item.thumbnails[format])?scope.item.thumbnails[format]:scope.item.url;
          
          
          
          
          
          scope.mediaType = scope.item.fileType;

          if(scope.itemToShow && scope.itemToShow.indexOf(scope.apiURI) >= 0){
            scope.apiURI = "";
          }
          if(scope.itemToShow && scope.itemToShow.indexOf(scope.apiURI+scope.apiURI) >= 0){
            scope.itemToShow.replace(scope.apiURI+scope.apiURI, scope.apiURI);
          }
          
          if(scope.item.fileType != 'FILE'){
            $scope.itemToShow = "/images/video.png";
            
            if(/(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(scope.item.url)){
              var matches = /(.+)\.youtube\.com\/watch\?v=(.+)/i.exec(scope.item.url);
              $scope.itemToShow = "https://i.ytimg.com/vi/"+matches[2]+"/hqdefault.jpg";
            }
          }
          
        }        
      }
    }
  })