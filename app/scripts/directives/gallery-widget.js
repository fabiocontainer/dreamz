'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('galleryWidget', function ($modal, $rootScope) {
    return {
      templateUrl: 'views/tpl/galeryWidget.html',
      restrict: 'E',
      link: function(scope, element, attrs){
        
       
        
        scope.removeGalItem = function(attachment, $event){
          if($event.preventDefault){ $event.preventDefault() }
          scope.content.Galery = scope.content.Galery.filter(function (item){
            if(item != attachment){
              return item;
            }
          })
          return false
        }
        scope.openGallery = function (event){
          if(event && event.preventDefault){event.preventDefault(); }
          $rootScope.$broadcast("ATTACHMENT_ADD", {callBack: "GALLERY_SELECT", contentType: 'images'});
          var eventCalled = $rootScope.$on("GALLERY_SELECT",  function (ev, item){
              if(item){
                if(!scope.content.Galery){
                  scope.content.Galery = [];
                }
                for(var i in item){
                  var attachment = item[i];
                  scope.content.Galery.unshift(attachment)
                }
                
                for(var i in scope.content.Galery){
                  scope.content.Galery[i]["order"] = parseInt(i);
                }

              }
              eventCalled();
            })
        }
        
      }
    }
  })