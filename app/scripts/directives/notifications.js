'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('notifications', function (Users, $rootScope) {
    return {
      templateUrl: 'views/tpl/notifications.html',
      restrict: 'E',
      link: function(scope, element, attrs){
        scope.alerts = [];
        scope.limit = 4;
        
        $rootScope.$on("NOTIFICATION", function (event, value){
          if((scope.alerts.length+1) >= scope.limit){
            scope.alerts = scope.alerts.slice(((scope.limit-1)*(-1)))
          }
          scope.alerts.push(value);
        })
        
        scope.closeAlert = function (alert){
          scope.alerts = scope.alerts.filter(function (item){
            if(item != alert){
              return item;
            }
          })
        }
      }
    }
  })