'use strict';

/**
 * @ngdoc directive
 * @name dreamzApp.directive:menu
 * @description
 * # menu
 */
angular.module('dreamzApp')
  .directive('imaginateDatePicker', function (Editorials, $rootScope) {
    return {
      templateUrl: 'views/tpl/date-picker.html',
      restrict: 'E',
      scope: {
        data: '='
      },
      link: function ($scope, element, attrs){
        $scope.opened = false;

        $scope.openDatePicker = function ($event){
          $event.preventDefault();
          $event.stopPropagation();

          $scope.opened = ($scope.opened == true)?false:true;
        }
      }
    };
  });
