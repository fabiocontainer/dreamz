'use strict';

/**
 * @ngdoc overview
 * @name dreamzApp
 * @description
 * # dreamzApp
 *
 * Main module of the application.
 */

 String.prototype.toSlug = function (){
   var replaces = {
     "ã": "a",
     "á": "a",
     "â": "a",
     "à": "a",
     "ä": "a",
     "ç": "c",
     "õ": "o",
     "ó": "o",
     "ô": "o",
     "ö": "o",
     "ò": "o",
     "ü": "u",
     "ú": "u",
     "ù": "u",
     "û": "u",
     " ": "-",
     "é": "e",
     "ê": "e",
     "ë": "e",
     "è": "e",
     "í": "i",
     "ï": "i",
     "ì": "i",
     "î": "i",
     "[:.!?,]": "",
     "(<([^>]+)>)": ""
   }
   var str = this.toLowerCase().trim();
   for(var i in replaces){
     var reg = new RegExp(i, "g")
     str = str.replace(reg, replaces[i]);
   }
   str = str.replace(/\s/ig,'');
   return str;
 }


angular
  .module('dreamzApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'ui.bootstrap',
    'pascalprecht.translate',
    'textAngular',
    'angular-loading-bar',
    'angularFileUpload'
  ])
  .constant('API_Configs', {
    'location': 'http://api.estrelando.com.br',
    //'location': 'http://localhost:3000',
    'secret': '$2a$10$dcYBaplfwtNbBNqlrKodS.Z1xFOlgJ4GduZu4HPECEyA.W5mC9VJ.',
    'client_id': '$2a$10$dcYBaplfwtNbBNqlrKodS.lJ7UD8FUustuVKjCEwn0kVQchByVG0S',

  })
  .config(function (timepickerConfig){
    timepickerConfig.meridians = true
    timepickerConfig.showMeridian = false
  })
  .config(function (datepickerConfig){
    datepickerConfig.datepickerMode = 'day'
  })
  .config(function ($translateProvider){
  	$translateProvider.translations('pt-br', ptBR)
  	$translateProvider.preferredLanguage('pt-br');
  })
  .config(function ($routeProvider) {
    for( var i in routes) {
      var route = routes[i];
      $routeProvider.when(route.route, route)
    }
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });
  })
  .factory('authHttpFailure',['API_Configs', '$q','$location', 'localStorageService', '$rootScope', function(API_Configs, $q,$location,localStorageService, $rootScope){
  	return {
  		response: function(response){
  		  if(response.status == 201 && response.config.url.indexOf(API_Configs.location) > -1)
        {
          $rootScope.$broadcast("NOTIFICATION", {type: 'success', msg: 'Saved Successfully'})
        }

  			return response || $q.when(response);
  		},
  		responseError: function(rejection) {

        if(rejection.status == 400 && rejection.config.url.indexOf(API_Configs.location) > -1)
        {
          $rootScope.$broadcast("NOTIFICATION", {type: 'alert', msg: ''})
        }
        if(rejection.status == 500 && rejection.config.url.indexOf(API_Configs.location) > -1)
        {
          $rootScope.$broadcast("NOTIFICATION", {type: 'danger', msg: 'Error to proccess request'})
        }

  			if (rejection.status === 401 || rejection.status === 403 ) {
  				var reg = new RegExp("\/oauth\/token");

  				//if(!reg.exec(rejection.config.url))
  				//{
  					$rootScope.$broadcast("HTTP_FORBIDDEN", rejection);
  				//}
  				localStorageService.remove("Dreamz_token");
  				//$rootScope.$broadcast("NOTIFICATION", {type: 'danger', msg: "Access Denied"});
  				//$location.path('/login');
  			}
  			return $q.reject(rejection);
  		}
  	}
  }])
  .factory('authHttp',['$q', 'localStorageService', '$location', '$rootScope', 'API_Configs', function($q, localStorageService, $location, $rootScope, API_Configs){
  	return {
  		request: function(config){
  		  if(config.url.indexOf(API_Configs.location) >=0 ){
  		    if(config.url != API_Configs.location+"/oauth/token") {
  		      var token = localStorageService.get("Dreamz_token");
      			if(token){
      				config.headers.Authorization = 'Bearer ' + token;
      			} else {
      				//localStorageService.remove('Dreamz_token');
      				//$rootScope.$broadcast("NOTIFICATION", {type: 'danger', msg: "Access Denied"});
      			}
  		    }

  		  }
  			return config || $q.when(config);
  		},
  	}
  }])
  .run(function ($rootScope){
    $rootScope.$off = function (name, listener){
      var namedListeners = this.$$listeners[name];
      if(namedListeners){
        for(var i = 0; i < namedListeners.length; i++){
          if(namedListeners[i] === listener){
            return namedListeners.splice(i, 1);
          }
        }
      }
    }
  })
  .config(['$httpProvider',function($httpProvider) {
  	$httpProvider.interceptors.push('authHttpFailure');
  	$httpProvider.interceptors.push('authHttp');
  	$httpProvider.defaults.headers.common.Authorization = 'Bearer ';
  }]);
